Algoritmo cajero_automatico
	
	//Arreglos (vectores)
	Dimension cuentas(3), claves(3), saldos(3);
	//Simular obtener desde base de datos
	cuentas[1] <- 1010
	cuentas[2] <- 2020
	cuentas[3] <- 3030
	claves[1] <- 1111
	claves[2] <- 2222
	claves[3] <- 3333
	saldos[1] <- 15000
	saldos[2] <- 25000
	saldos[3] <- 25000
	
	//Inicia el sistema de cajero para retiros	
	Escribir "Escriba # de la tarjeta "
	leer tarjeta
	contador <- 0
	
	Para i <- 1 Hasta 3 Con Paso 1 Hacer
		si cuentas[i] = tarjeta Entonces
			clave <- claves[i]
			saldo <- saldos[i]
			Retirar(clave, saldo) // Llamar a la funci�n
		SiNo
			contador <- contador + 1
		FinSi
	FinPara
	Si contador > 2 Entonces
		Escribir "Problemas con lectura o # de tarjeta, vuelve a intertarlo"
	Sino
		Escribir "Gracias por preferirnos"
	FinSi
FinAlgoritmo // OJO miren donde finaliza el algoritmo

//Funciones - M�todos
Funcion Retirar(clave,saldo)
	Escribir "ingrese monto a retirar"
	leer monto
	Si monto > saldo Entonces
		Escribir "Lo sentimos, saldo insuficiente, vuelve a internarlo"
	SiNo
		Escribir "Ingrese la clave"
		leer contrase�a
		Si contrase�a = clave Entonces
			Escribir "Bien, el cajero est� contando: $ " monto
			total <- saldo - monto
			Escribir "Su saldo actual es: " total
		SiNo
			Escribir "Lo sentimos, clave incorrecta"
		FinSi
	FinSi
FinFuncion